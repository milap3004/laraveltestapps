<!DOCTYPE html>
<html>
<head>
	{!! Html::script( asset('js/jquery-2.1.3.min.js')) !!}
    <script type="text/javascript">	
			function jsCreateArticle(){
				dataString = '_token=LWPZV8jsdzoN4xxF6EoSrA0E1VKkQIPTKdyIWkv5';
				$.ajax({
				 type: "GET",
				 url : "article/create",
				 data : dataString,
				 dataType : "json",
				 success : function(data){
					$('#ssIdDivContent').html(data['html']);
				}				 
				},"json");				
				return false;
			}
			
			function jsListArticle(){
				dataString = '_token=LWPZV8jsdzoN4xxF6EoSrA0E1VKkQIPTKdyIWkv5'; 				
				$.ajax({
				 type: "GET",
				 url : "article",
				 data : dataString,
				 dataType : "json",
				 success : function(data){
					$('#ssIdDivContent').html(data['html']);
				}				 
				},"json");				
				return false;
			}
			
	</script>
	@include('article/_tophead',array('heading_text' => 'Article List'))
</head>
<body>
<div class="container">	
	<div id="ssIdDivContent">
		@include('article/_list',['amArticle'=>$amArticle])
	</div>
</div>
</body>
