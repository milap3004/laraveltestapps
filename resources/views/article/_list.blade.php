	

	    <script type="text/javascript">	  
			function jsViewArticle(snJsIdView){
				dataString = '_token='+$( this ).find( 'input[name=_token]' ).val();
				$.ajax({
				 type: "GET",
				 url : "article/"+snJsIdView,
				 data : dataString,
				 dataType : "json",
				 success : function(data){
					$('#ssIdDivContent').html(data['html']);
				}				 
				},"json");				
				return false;
			}

			function jsEditArticle(snJsIdView){
				dataString = '_token=LWPZV8jsdzoN4xxF6EoSrA0E1VKkQIPTKdyIWkv5';
				$.ajax({
				 type: "GET",
				 url : "article/"+snJsIdView+"/edit",
				 data : dataString,
				 dataType : "json",
				 success : function(data){
					$('#ssIdDivContent').html(data['html']);
				}				 
				},"json");				
				return false;
			}

		//jQuery(document).ready(function($){
			$('#frmArticleDelete').on('submit', function(){ 
				$.post(
					$(this).prop('action'),$("#frmArticleDelete").serialize(),
					function(data){						
						$('#ssIdDivContent').html(data['html']);
						//response after the process. 
					},
					'json'
				);   
			});
		//});
	</script>

	

	@include('article/_articleTopLink',['article_title_text'=>'Listing'])
	@if (Session::has('message'))
		<div class="alert alert-info">{{ Session::get('message') }}</div>
	@endif
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<td> Id </td> 
				<td width="7%"> Title </td> 
				<td width="40%">Article Description</td>
				<td width="7%">Added Date</td>
				<td width="7%">Change Date</td>			
				<td>Event Perform</td>
			</tr>
		</thead>
		<tbody>
		@foreach($amArticle as $key => $value)
			<tr>
				<td> {{ $value->id }} </td> 
				<td> {{ $value->title }} </td> 
				<td> {{ $value->description }} </td>
				<td> {{ $value->created_at }} </td>
				<td> {{ $value->change_date }} </td>			
				<!-- we will also add show, edit, and delete buttons -->
				<td>
					{!! Form::open(array('url' => 'article/'. $value->id, 'class' => 'pull-right','id'=>'frmArticleDelete')) !!}
						{!! Form::hidden('_method', 'DELETE') !!}
						{!! Form::submit('Delete Article', array('class' => 'btn btn-warning')) !!}
					{!! Form::close() !!}
					<!-- <a class="btn btn-small btn-success" href="{{ URL::to('article/'.$value->id) }}">View Article</a> -->
					<a class="btn btn-small btn-success" href="javascript:void(0);"  onclick="jsViewArticle('<?php echo $value->id; ?>');">View Article</a>
					<!-- <a class="btn btn-small btn-info" href="{{ URL::to('article/'.$value->id.'/edit') }}">Edit Article</a> -->
					<a class="btn btn-small btn-info" href="javascript:void(0);"  onclick="jsEditArticle('<?php echo $value->id; ?>');">Edit Article</a>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
