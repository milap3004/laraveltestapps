<!DOCTYPE html>
<html>
<head>
	@include('article/_tophead',array('heading_text' => 'Edit Article'))
</head>
<body>
<div class="container">
		<div id="ssIdDivContent">		
			@include('article/_addedit',['ssEvent'=>'Edit','errors'=>$errors,'ssTitle'=>$article->title,'ssDescription'=>$article->description,'article'=>$article])
		</div>
<!--
 	 <div class="form-group">
 	 	 {!! Form::label('title', 'Title') !!}
 	 	 {!! Form::text('title', $article->title, array('class' => 'form-control')) !!}
			@if($errors->first('title') )
				{{ $errors->first('title') }}
			@endif	 
 	 </div>
 	 <div class="form-group">
 	 	 {!! Form::label('description', 'Description') !!}
 	 	 {!! Form::textarea('description',$article->description) !!}
			@if($errors->first('description') )
				{{ $errors->first('description') }}
			@endif		 
 	 </div>
{!! Form::submit('Edit the Nerd!', array('class' => 'btn btn-primary')) !!}
{!! Form::close() !!} 
-->
</div>
</body>
</html>