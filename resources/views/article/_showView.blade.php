@include('article/_articleTopLink',['article_title_text'=>'Article View'])
<div class="jumbotron text-center">
	<table class="table table-striped table-bordered">
		<tr>
			<td width="20%" align="right"><strong> Title : </strong></td>
			<td align="left">{{ $article->title }}</td>			
		<tr>
		<tr>
			<td width="20%" align="right"><strong> Description : </strong></td>
			<td align="left">{{ $article->description }}</td>			
		<tr>
		<tr>
			<td width="20%" align="right"><strong> Change Date : </strong></td>
			<td align="left">{{ $article->change_date }}</td>			
		<tr>
	</table>
</div>
