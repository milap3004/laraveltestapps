	<script type="text/javascript">	
		jQuery(document).ready(function($){
			$('#frmArticleAddEdit').on('submit', function(){ 		 
			   // ajax post method to pass form data to the 			   
        $.post(
            $(this).prop('action'),$("#frmArticleAddEdit").serialize(),			
            function(data){				
				$('#ssIdDivContent').html(data['html']);
                //response after the process. 
            },
            'json'
        );   
			   
				return false;
			});  
		});	
	</script>

	@if($ssEvent == 'Add')
		@include('article/_articleTopLink',['article_title_text'=>'Create a Article'])	
		{!! Form::open(array('url' => 'article','id'=>'frmArticleAddEdit','name'=>'frmArticleAddEdit')) !!}
	@endif

	@if($ssEvent == 'Edit')
		@include('article/_articleTopLink',['article_title_text'=>'Edit a Article'])
		{!! Form::model('article', array('route' => array('article.update', $article->id), 'method' => 'PUT','id'=>'frmArticleAddEdit','name'=>'frmArticleAddEdit')) !!}
	@endif
	
	 <div class="form-group">
 	 	 {!! Form::label('title', 'Title') !!}
 	 	 {!! Form::text('title', $ssTitle, array('class' => 'form-control')) !!}
			@if(isset($errors) && $errors->first('title') )
				{{ $errors->first('title') }}
			@endif	 
 	 </div>
 	 <div class="form-group">
 	 	 {!! Form::label('description', 'Description') !!}
 	 	 {!! Form::textarea('description',$ssDescription) !!}
			@if(isset($errors) && $errors->first('description') )
				{{ $errors->first('description') }}
			@endif		 
 	 </div>

		@if($ssEvent == 'Add')
			{!! Form::submit('Create', array('class' => 'btn btn-primary')) !!}
		@endif

		@if($ssEvent == 'Edit')		
			{!! Form::submit('Edit the Nerd!', array('class' => 'btn btn-primary')) !!}
		@endif
		
{!! Form::close() !!}
