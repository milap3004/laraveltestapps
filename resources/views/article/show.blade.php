<!DOCTYPE html>
<html>
<head>	
	@include('article/_tophead',array('heading_text' => 'Show Article'))
</head>
<body>
<div class="container">		
	<div id="ssIdDivContent">
		@include('article/_showView',['article'=>$article])
	</div>

</div>
</body>
</html>