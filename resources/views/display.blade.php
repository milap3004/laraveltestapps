@extends('app')
@section('head')

@stop

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
{!! Html::script( asset('js/localGoogleCode.js')) !!}

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Calculate Distance By Googe API of Distance Calculator version 3</div>
				<div class="panel-body">
				</div>
			</div>
		</div>
	</div>
</div>


{!! Form::open(array('id' => 'form'));  !!}
		<table align="center" valign="center">
			<tr>
				<td colspan="7" align="center"><b>Find the distance between two locations</b></td>
			</tr>
			<tr>
				<td colspan="7">&nbsp;</td>
			</tr>
			<tr>
				<td>First address:</td>
				<td>&nbsp;</td>
				<td>{!! Form::text('address1', '',array('id'=>'address1','size'=>'50'));  !!}</td>
				<td>&nbsp;</td>
				<td>Second address:</td>
				<td>&nbsp;</td>				
				<td>{!! Form::text('address2', '',array('id'=>'address2','size'=>'50'));  !!}</td>
			</tr>
			<tr>
				<td colspan="7">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="7" align="center">{!! Form::button('Show me Distance',array('onclick'=>'initialize()'));   !!}</td>
			</tr>
		</table>
	</div>
{!! Form::close() !!}
	<center><div style="width:100%; height:10%" id="distance_direct"></div></center>
	<center><div style="width:100%; height:10%" id="distance_road"></div></center>
	
	<center><div id="map_canvas" style="width:70%; height:54%"></div></center>

	
@endsection