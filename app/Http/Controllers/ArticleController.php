<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;

/**
* Defining by manully
*/
use App\Article;
use View;
use Session;
use Redirect;
use Validator;
use Input;
use Request;
use Response;

class ArticleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$amArticle = Article::all();
		if (Request::ajax()) 
		{
			$html = View::make('article/_list', ['amArticle'=>$amArticle])->render();
			return Response::json(['html' => $html]);  
			exit;
		} 
		else 
		{
			return View::make('article.index')->with('amArticle',$amArticle);
		}	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if (Request::ajax()) 
		{
			$html = View::make('article/_addedit', ['ssEvent'=>'Add','ssTitle'=>'','ssDescription'=>''])->render();
			return Response::json(['html' => $html]);  
			exit;
		} 
		else 
		{
			return View::make('article.create');
		}	
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//return view('article/_addedit');
		/*
		$title = Input::get('title');  
		$description = Input::get('description');        
		$validateArticle = array(
			"title" => "required|min:5|max:30"  ,
			"description" => "required"  ,
		);
		*/
		
		$validator = Validator::make(Input::all(),Article::$validateArticle);
		
		
        if ($validator->fails()) 
		{
			$messages = $validator->messages();					
			//$html =  view('updateJqueryDisplay')->render();
			//$html = View::make('countries.list', compact('countries'))->render();
			//return response()->json($contents);		
		
			$html = View::make('article/_addedit', ['ssEvent'=>'Add','validator' => $validator,'errors' => $messages,'ssTitle'=>Input::get('title'),'ssDescription'=>Input::get('description')])->render();
			return Response::json(['html' => $html]);  
			exit;
			/*		
			return Redirect::back()
                ->withErrors($validator)
				->withInput(Input::all())
                ;
			*/
        }
		
		$article = new Article;
		$article->title = Input::get("title");
		$article->description = Input::get("description");
		$article->change_date = date("Y-m-d");		 
		$article->save();
		Session::flash('message', 'Article Created Successfully');

		$amArticle = Article::all();
		$html = View::make('article/_list',['amArticle' => $amArticle])->render();
        return Response::json(['html' => $html]);  
		exit;
        //return Redirect::to('article');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$article = Article::find($id);
		if (Request::ajax()) 
		{
			$html = View::make('article/_showView', ['article'=>$article])->render();
			return Response::json(['html' => $html]);  
			exit;
		} 
		else 
		{
			return View::make('article.show')->with('article',$article );
		}	
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$article = Article::find($id);	

		$html = View::make('article/_addedit',['ssEvent'=>'Edit','ssTitle'=>$article->title,'ssDescription'=>$article->description,'article'=>$article])->render();
		return Response::json(['html' => $html]);  
		exit;		
/*		
        return View::make('article.edit')
            ->with('article',$article);
*/
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{ 
		$validator = Validator::make(Input::all(),Article::$validateArticle);
        if ($validator->fails()) 
		{
			$messages = $validator->messages();					
			$html = View::make('article/_addedit', ['ssEvent'=>'Add','validator' => $validator,'errors' => $messages,'ssTitle'=>Input::get('title'),'ssDescription'=>Input::get('description')])->render();
			return Response::json(['html' => $html]);  
			exit;
        }
		
		$article = Article::find($id);
		$article->title = Input::get("title");
		$article->description = Input::get("description");
		$article->change_date = date("Y-m-d");		 
		$article->save();
		Session::flash('message', 'Article Updated Successfully');

		$amArticle = Article::all();
		$html = View::make('article/_list',['amArticle' => $amArticle])->render();
        return Response::json(['html' => $html]);  
		exit;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{		
		echo $id; exit;
       // delete
        $article = Article::find($id);
       // $article->delete();
        // redirect		
        Session::flash('message', 'Article Deleted Successfully');
        //return Redirect::to('article');		

		$amArticle = Article::all();
		$html = View::make('article/_list',['amArticle' => $amArticle])->render();
        return Response::json(['html' => $html]);  
		exit;
		
	}

}
