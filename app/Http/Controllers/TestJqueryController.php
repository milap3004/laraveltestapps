<?php namespace App\Http\Controllers;


//use Response;
use Request;
use View;

class TestJqueryController extends Controller {

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function updateJqueryDisplay()
	{
		if (Request::ajax()) {
		//	return Response::json('aaaaaaaaaaaaa');
			return response()->json(['name' => 'Abigail', 'state' => 'CA']);
			//return view('updateJqueryDisplay');
		} else {
			return View::make('updateJqueryDisplay');
			//return view('updateJqueryDisplay');
		}	
	}

}
