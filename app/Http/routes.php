<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('distrancefind', 'DistranceFindController@index');
Route::get('jsnList', 'TestJqueryController@updateJqueryDisplay');

Route::post('TestJquery/updateJqueryDisplay', 'TestJqueryController@updateJqueryDisplay');
Route::get('TestJquery/updateJqueryDisplay', 'TestJqueryController@updateJqueryDisplay');


Route::resource("article","ArticleController");
Route::post('article/create', ['uses' => 'ArticleController@create']);
//Route::resource("/","ArticleController");


Route::get('home', 'HomeController@index');
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
