<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {

	  public static $validateArticle = array(
      "title" => "required|min:5|max:30"  ,
      "description" => "required"  ,
    );

}
